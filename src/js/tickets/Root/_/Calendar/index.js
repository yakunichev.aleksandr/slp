import React from 'react';
import T from 'prop-types';
import cx from 'classnames';
import {compose, withStateHandlers, withHandlers, setPropTypes, onlyUpdateForPropTypes} from 'recompose';
import {startOfToday, isBefore, parse, eachDay, differenceInDays, isToday, isEqual, addDays, format, isFirstDayOfMonth, getISODay} from 'date-fns';
import {findIndex,always, filter, range, map, partial, flatten} from 'ramda';

export const Item = ({isToday, date, isCurrent, isPast, isMonth, onClick}) => isMonth ? (
  <div className="tickets__calendar-date month">
    <div>
      <span>
        <svg viewBox="0 0 18 12">
          <g fill="none" fillRule="evenodd">
            <path stroke="#FAEAD3" strokeWidth="2" d="M10.081 1L16 5.966 10 11"/>
            <path fill="#FAEAD3" d="M0 5h16v2H0z"/>
          </g>
        </svg>
      </span>
      <small>{format(date, 'MMM')}</small>
    </div>
  </div>
) : (
  <button onClick={() => !isPast && onClick && onClick(date)} type="button" className={cx('tickets__calendar-date', {today: isToday, current: isCurrent, past: isPast})}>
    <div>
      <span>{format(date, 'DD')}</span>
      <small>{format(date, 'dd')}</small>
    </div>
  </button>
);

const today = startOfToday();

export const Calendar = ({from, to, current, choosenDate, onChange, onPrev, onNext}) => (
  <div className="tickets__calendar">
    <button type="button" className="tickets__calendar-nav-left" onClick={onPrev}><div/><div/></button>

    <div className="tickets__calendar-wrapper">
      <div className="tickets__calendar-container" style={{transform: `translateX(${(-current + 5) * (window.innerWidth < 770 ? 25 : 7.142857143)}%)`}}>
        {compose(
          flatten,
          filter(Boolean),
          map(date => [
            <Item
              key={date}
              date={date}
              isToday={isToday(date)}
              isCurrent={isEqual(date, choosenDate)}
              isPast={isBefore(date, today)}
              onClick={onChange}
              />,
            isFirstDayOfMonth(date) ?
              <Item
                isMonth
                date={date}
                key={format(date, 'MM')}
                />
              : null,
          ]),
          map(format),
        )(eachDay(from, to))}
      </div>
    </div>

    <button type="button" className="tickets__calendar-nav-right" onClick={onNext}><div/><div/></button>
  </div>
);

export const enhance = compose(
  withStateHandlers(
    props => ({current: findIndex(isToday, eachDay(props.from, props.to)), length: eachDay(props.from, props.to).length}),
    {onNext: ({current, length}) => () => ({current: Math.min(current + (window.innerWidth < 770 ? 4 : 7), length)}),
      onPrev: ({current}) => () => ({current: Math.max(current - (window.innerWidth < 770 ? 4 : 7), 0)})},
  ),
  onlyUpdateForPropTypes,
  setPropTypes({
    from: T.string.isRequired,
    to: T.string.isRequired,
    current: T.number.isRequired,
    choosenDate: T.string.isRequired,
    onChange: T.func,
    onNext: T.func.isRequired,
    onPrev: T.func.isRequired,
  }),
);

export default enhance(Calendar);
