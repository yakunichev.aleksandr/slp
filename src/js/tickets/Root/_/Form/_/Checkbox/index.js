import React from 'react';
import T from 'prop-types';
import {omit} from 'ramda';

export const Checkbox = props => (
  <label className="tickets__checkbox">
    <input className="tickets__checkbox-input" type="checkbox" {...omit(['label'], props)}/>
    <span className="tickets__checkbox-label">{props.label}</span>
  </label>
);

export default Checkbox;
