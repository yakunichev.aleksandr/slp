const $elements = Array.prototype.slice.call(document.querySelectorAll('[data-animation]'));

window.scrollTo(0, 0);

export const animate = $el => $el.classList.add('animated');

export const isInViewport = $el => {
  const rect = $el.getBoundingClientRect();
  return rect.top - window.innerHeight / 1.5 < 0;
};

export const onScroll = () =>
  $elements.forEach($el => isInViewport($el) ? animate($el) : null);

window.addEventListener('scroll', onScroll, {passive: true});
window.addEventListener('resize', onScroll, {passive: true});
window.addEventListener('load', onScroll, {passive: true});
onScroll();
