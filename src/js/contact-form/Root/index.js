import React from 'react';
import T from 'prop-types';
import {compose, withHandlers, withStateHandlers, lifecycle, onlyUpdateForPropTypes, setPropTypes} from 'recompose';
import * as Modules from './_';
import * as api from '../../api';

export const options = [{
  value: 'fr',
  label: 'Франшиза',
}, {
  value: 'vac',
  label: 'Вакансии',
}, {
  value: 'comp',
  label: 'Жалоба',
}];

export const Root = ({
  theme,
  name,
  email,
  phone,
  message,
  onThemeChange,
  onNameChange,
  onEmailChange,
  onPhoneChange,
  onMessageChange,
  onClose,
  onSubmit,
}) =>  (
  <div className="contact-form">
    <div onClick={onClose} className="contact-form__overlay"/>
    <div className="contact-form__container">
      <form className="contact-form__form" onSubmit={onSubmit}>
        <header className="contact-form__header">
          <h3 className="contact-form__title">Обратная связь</h3>
          <button className="contact-form__close" type="button" onClick={onClose}>
            <svg viewBox="0 0 32 32">
              <g fillRule="evenodd">
                <path d="M14.936 16.704l1.768-1.768 15.04 16.78z"/>
                <path d="M16.704 16.704l-1.768-1.768 16.78-15.04z"/>
                <path d="M14.936 14.936l1.768 1.768-16.78 15.04z"/>
                <path d="M16.704 14.936l-1.768 1.768L-.104-.076z"/>
              </g>
            </svg>
          </button>
        </header>

        <div className="contact-form__fieldset">
          <Modules.Select
            value={theme}
            options={options}
            label="Тема письма"
            onChange={onThemeChange}
            />

          <Modules.Input
            required
            autoFocus
            name="name"
            label="Имя"
            value={name}
            autoComplete="name"
            onChange={onNameChange}
            />
        </div>

        <div className="contact-form__fieldset">
          <Modules.Input
            required
            type="email"
            name="email"
            value={email}
            label="E-mail"
            autoComplete="email"
            onChange={onEmailChange}
            />

          <Modules.Input
            required
            type="tel"
            name="tel"
            value={phone}
            label="Телефон"
            autoComplete="tel"
            onChange={onPhoneChange}
            />
        </div>

        <div className="contact-form__fieldset">
          <Modules.Textarea
            required
            name="message"
            value={message}
            label="Текст письма"
            autoComplete="message"
            onChange={onMessageChange}
            />
        </div>

        <div className="contact-form__fieldset">
          <button className="contact-form__submit" type="submit">Отправить</button>
        </div>
      </form>
    </div>
  </div>
);

export const enhance = compose(
  withStateHandlers(
    () => ({theme: options[0].value, name: '', email: '', phone: '', message: ''}), {
      onNameChange: () => ev => ({name: ev.target.value}),
      onEmailChange: () => ev => ({email: ev.target.value}),
      onPhoneChange: () => ev => ({phone: ev.target.value}),
      onMessageChange: () => ev => ({message: ev.target.value}),
      onThemeChange: () => theme => ({theme}),
    }
  ),
  lifecycle({
    componentDidMount() {
      document.body.style.overflow = 'hidden';
    },
    componentWillUnmount() {
      document.body.style.overflow = '';
    }
  }),
  withHandlers({
    onSubmit: ({theme, name, email, phone, message}) => ev => {
      ev.preventDefault();
      api.contact({theme, name, email, phone, message});
    },
  }),
  onlyUpdateForPropTypes,
  setPropTypes({
    theme: T.string.isRequired,
    name: T.string.isRequired,
    email: T.string.isRequired,
    phone: T.string.isRequired,
    message: T.string.isRequired,
    onThemeChange: T.func.isRequired,
    onNameChange: T.func.isRequired,
    onEmailChange: T.func.isRequired,
    onPhoneChange: T.func.isRequired,
    onMessageChange: T.func.isRequired,
    onClose: T.func.isRequired,
  }),
);

export default enhance(Root);
