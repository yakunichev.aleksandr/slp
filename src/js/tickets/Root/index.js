import React from 'react';
import T from 'prop-types';
import * as Modules from './_';
import {format} from 'date-fns';
import {compose, withState, lifecycle, withHandlers, withStateHandlers, onlyUpdateForPropTypes, setPropTypes} from 'recompose';
import {always} from 'ramda';
import * as api from '../../api';

export const Root = ({
  choosenDate,
  onDateChange,
  interval,

  sessions,
  choosenTime,
  onTimeChange,
  isSessionsLoading,

  isLoading,
  onSubmit,
}) => (
  <section className="container">
    {interval ? (
      <Modules.Calendar
        {...interval}
        choosenDate={choosenDate}
        onChange={isSessionsLoading ? null : onDateChange}
        />
    ) : null}
    <Modules.Sessions
      sessions={sessions}
      choosenTime={choosenTime}
      loading={isSessionsLoading}
      onTimeChange={onTimeChange}
      />
    <Modules.Form
      isLoading={isLoading}
      onSubmit={onSubmit}
      />
  </section>
);

export const enhance = compose(
  withStateHandlers(
    always({choosenDate: format(new Date(), 'YYYY-MM-DD')}),
    {onDateChange: () => choosenDate => ({choosenDate})},
  ),
  withStateHandlers(
    always({choosenTime: null}),
    {onTimeChange: () => choosenTime => ({choosenTime})},
  ),
  withStateHandlers(
    always({sessions: [], isSessionsLoading: false}),
    {onSessionsChange: () => sessions => ({sessions}),
     onSessionsLoadingChange: () => isSessionsLoading => ({isSessionsLoading})},
  ),
  withState('isLoading', 'onLoadingChange', false),
  withState('interval', 'onIntervalChange', null),
  withHandlers({
    loadSessions: props => () => {
      props.onSessionsLoadingChange(true);
      props.onTimeChange(null);
      api.sessions.get(props.choosenDate).then(data => {
        props.onSessionsLoadingChange(false);
        props.onSessionsChange(data);
      });
    },
    onSubmit: props => ({name, email, phone}) => {
      props.onLoadingChange(true);
      api.sessions.post({name, email, phone, time: props.choosenTime, date: props.choosenDate});
    },
  }),
  withHandlers({
    onDateChange: props => date => {
      props.onDateChange(date);
      props.loadSessions();
    },
  }),
  lifecycle({
    componentDidMount() {
      api.sessions.interval().then(interval => {
        this.props.onIntervalChange(interval);
        this.props.loadSessions();
      });
    },
  }),
  onlyUpdateForPropTypes,
  setPropTypes({
    interval: T.shape({
      from: T.string.isRequired,
      to: T.string.isRequired,
    }),
    isLoading: T.bool.isRequired,
    isSessionsLoading: T.bool.isRequired,
    sessions: T.array.isRequired,
    choosenTime: T.string,
    choosenDate: T.string.isRequired,
    onDateChange: T.func.isRequired,
    onTimeChange: T.func.isRequired,
  }),
);

export default enhance(Root);
