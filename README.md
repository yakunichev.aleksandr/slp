# Project initialization
Before running the project you should install all dependencies:
```sh
yarn # npm install
```

To run the project in development mode:
```sh
yarn watch # npm run watch
```

To build the project:
```sh
yarn build # npm run build
```

[Test server url](http://slp.yakunichevaleksandr.com);

## Api
All API endpoints for now is mocked, but you can find real API requests code commented under.

To enable API – just uncomment that code and set the right urls and headers for the appliaction.

All API code is here: `src/js/api`.
