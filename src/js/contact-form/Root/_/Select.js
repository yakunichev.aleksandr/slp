import React from 'react';
import T from 'prop-types';
import {compose, lifecycle, setPropTypes, onlyUpdateForPropTypes, withHandlers, withState} from 'recompose';

export const Option = withHandlers({
  onClick: props => () => props.onClick(props.value),
})(({label, onClick}) => (
  <div className="contact-form__select-option" onClick={onClick}>{label}</div>
));

export const Select = ({
  label,
  value,
  options,
  isOpen,
  onOpen,
  onClose,
  onChange,
}) => (
  <label className="contact-form__input">
    <span className="contact-form__input-label" onClick={isOpen ? onClose : onOpen}>{label}</span>
    <div className="contact-form__input-select" onClick={isOpen ? onClose : onOpen}>{options.find(v => v.value === value).label}</div>
    {isOpen ? (
      <div className="contact-form__select-dropdown">
        {options.map(option => <Option {...option} key={option.value} onClick={onChange}/>)}
      </div>
    ) : null}
  </label>
);

export const enhance = compose(
  withState('isOpen', 'changeIsOpen', false),
  withHandlers({
    onChange: props => (...args) => {
      props.changeIsOpen(false);
      props.onChange(...args);
    },
    onOpen: props => () => props.changeIsOpen(true),
    onClose: props => () => props.changeIsOpen(false),
  }),
  onlyUpdateForPropTypes,
  setPropTypes({
    value: T.string.isRequired,
    options: T.arrayOf(T.shape({
      label: T.string.isRequired,
      value: T.string.isRequired,
    }).isRequired).isRequired,
    isOpen: T.bool.isRequired,
    onOpen: T.func.isRequired,
    onClose: T.func.isRequired,
    onChange: T.func.isRequired,
  }),
);

export default enhance(Select);
