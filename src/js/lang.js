const $button = document.querySelector('[data-header-lang]');

export const onClick = () =>
  $button.classList.toggle('active');

$button.addEventListener('click', onClick, {passive: true});
