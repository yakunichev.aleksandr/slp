import axios from 'axios';
import {prop, range} from 'ramda';
import {parse, addMinutes, format} from 'date-fns';

export const instance = axios.create({
  // baseURL: 'https://some-domain.com/api/',
});

export const sessions = {
  interval: () =>
    new Promise(resolve => setTimeout(() => resolve({from: '2018-07-15', to: '2018-09-15'}), 1000)),
    // instance.get('/api/booking_interval').then(prop('data')),
  get: day =>
    new Promise(resolve => setTimeout(() => resolve(range(0, 21).map(index => ({
      datetime: '2018-08-20-10-30',
      date: '2018-08-20',
      time: format(addMinutes(new Date('August 07, 2018 12:00:00'), index * 30), 'HH:mm'),
      price: 4000,
      free: true,
    }))), 1000)),
    //instance.get('/api/session', {params: {day}}).then(prop('data')),
  post: ({date, time, name, phone, email}) =>
    new Promise(resolve => setTimeout(() => resolve(), 1000)),
    // instance.post('/api/booking', {date, time, name, phone, email}).then(prop('data'))
};

export const contact = ({theme, name, email, phone, message}) =>
  new Promise(resolve => setTimeout(() => resolve(), 1000));
  // instance.post('/api/message', {theme, name, email, phone, message}).then(prop('data'));
