const path = require('path');
const context = __dirname;

module.exports = {
  plugins: [
    require('postcss-import')({path: context}),
    require('postcss-clearfix'),
    require('postcss-cssnext'),
    require('cssnano'),
  ]
};
