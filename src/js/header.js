const $nav = document.querySelector('[data-nav]');
const $trigger = document.querySelector('[data-nav-trigger]');
const $links = Array.prototype.slice.call(document.querySelectorAll('[data-header-link]'));
let isOpen = false;

const onClick = () => {
  if (isOpen) {
    $nav.classList.remove('active');
    $trigger.classList.remove('active');
    document.body.style.overflow = '';
  } else {
    $nav.classList.add('active');
    $trigger.classList.add('active');
    document.body.style.overflow = 'hidden';
  }
  isOpen = !isOpen;
};


$trigger.addEventListener('click', onClick, {passive: true});
$links.forEach($link => $link.addEventListener('click', onClick, {passive: true}));
