import React from 'react';
import {omit} from 'ramda';

export const Textarea = props => (
  <label className="contact-form__input full">
    <span className="contact-form__input-label">{props.label}</span>
    <textarea className="contact-form__input-textarea" {...omit(['label'], props)}/>
  </label>
);

export default Textarea;
