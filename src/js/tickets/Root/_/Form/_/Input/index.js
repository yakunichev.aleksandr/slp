import React from 'react';
import T from 'prop-types';
import {omit} from 'ramda';

export const Input = props => (
  <label className="tickets__input">
    <span className="tickets__input-label">{props.placeholder}</span>
    <input className="tickets__input-input" {...omit(['placeholder'], props)}/>
  </label>
);

export default Input;
