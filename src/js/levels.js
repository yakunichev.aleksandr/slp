const $root = document.getElementById('levels-slider');
const $container = $root.querySelector('[data-container]');
const $slides = Array.prototype.slice.call($root.querySelectorAll('[data-slide]'));
const $prev = $root.querySelector('[data-prev]');
const $next = $root.querySelector('[data-next]');
const $paginations = Array.prototype.slice.call($root.querySelectorAll('[data-pagination]'));

let activeIndex = 0;

const onChange = () => {
  $container.style.transform = `translateX(calc(${(activeIndex * 100 * -1)}% - ${activeIndex * 66}px))`;
  $paginations.forEach(($el, index) =>
    $el.classList[index === activeIndex ? 'add' : 'remove']('active')
  );
  $slides.forEach(($el, index) =>
    $el.classList[index === activeIndex ? 'add' : 'remove']('active')
  );
};

const onPrev = () => {
  activeIndex = activeIndex - 1 < 0 ? $slides.length - 1 : activeIndex - 1;
  onChange();
};
const onNext = () => {
  activeIndex = activeIndex + 1 > $slides.length - 1 ? 0 : activeIndex + 1;
  onChange();
};

const onPaginationClick = index => () => {
  activeIndex = index;
  onChange();
};

$prev.addEventListener('click', onPrev, {passive: true});
$next.addEventListener('click', onNext, {passive: true});
$paginations.forEach(($el, i) => $el.addEventListener('click', onPaginationClick(i), {passive: true}));
