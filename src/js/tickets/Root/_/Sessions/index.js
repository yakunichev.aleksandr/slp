import React from 'react';
import T from 'prop-types';
import cx from 'classnames';
import {compose, setPropTypes, onlyUpdateForPropTypes} from 'recompose';
import {range} from 'ramda';
import {isBefore, format, parse} from 'date-fns';

const today = new Date();

export const Session = ({date, time, price, choosenTime, onClick}) => (
  <li className={cx('tickets__sessions-item', {active: choosenTime === time, disabled: isBefore(date, today)})}>
    <button className="tickets__sessions-button" disabled={isBefore(parse(date), parse(today))} type="button" onClick={() => onClick(time)}>{time}</button>
    <p className="tickets__sessions-price">{`${price} rub`}</p>
  </li>
);

export const Sessions = ({loading, sessions, choosenTime, onTimeChange}) => loading ? (
  <div className="tickets__sessions-placeholder">
    Loading
  </div>
) : (
  <ul className="tickets__sessions">
    {sessions.map((item, index) => (
      <Session
        {...item}
        key={index}
        choosenTime={choosenTime}
        onClick={onTimeChange}
        />
    ))}
  </ul>
);

export const enhance = compose(
  onlyUpdateForPropTypes,
  setPropTypes({
    loading: T.bool.isRequired,
    choosenTime: T.string,
    sessions: T.arrayOf(T.shape({
      date: T.string.isRequired,
      time: T.string.isRequired,
      price: T.number.isRequired,
    }).isRequired).isRequired,
    onTimeChange: T.func.isRequired,
  }),
);

export default enhance(Sessions);
