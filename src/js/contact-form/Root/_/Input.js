import React from 'react';
import T from 'prop-types';
import {omit} from 'ramda';

export const Input = props => (
  <label className="contact-form__input">
    <span className="contact-form__input-label">{props.label}</span>
    <input className="contact-form__input-input" {...omit(['label'], props)}/>
  </label>
);

export default Input;
