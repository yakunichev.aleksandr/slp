const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = env => ({
  mode: env.production ? 'production' : 'development',
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  module: {
    rules: [
      {test: /\.js/, loader: 'babel-loader'},
      {test: /\.hbs/, loader: 'handlebars-loader'},
      env.production ?
        {test: /\.css$/, use: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader!postcss-loader'})} :
        {test: /\.css$/, loader: 'style-loader!css-loader!postcss-loader'},
    ],
  },
  context: path.resolve(__dirname, 'src'),
  target: 'web',
  devServer: env.production ? {} : {
    hot: true,
    inline: true,
    publicPath: '/',
    host: '0.0.0.0',
    contentBase: path.resolve(__dirname, 'dist'),
  },
  plugins: env.production ? [
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'dist/index.html'),
      template: path.resolve(__dirname, 'src/index.hbs'),
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        conservativeCollapse: true
      },
    }),
    new ExtractTextPlugin('styles.css'),
  ] : [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'dist/index.html'),
      template: path.resolve(__dirname, 'src/index.hbs'),
    }),
  ],
});
