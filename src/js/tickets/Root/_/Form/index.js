import React from 'react';
import cx from 'classnames';
import T from 'prop-types';
import {compose, withStateHandlers, withHandlers, setPropTypes, onlyUpdateForPropTypes} from 'recompose';
import {always} from 'ramda';
import * as Modules from './_';

export const Form = ({
  name,
  email,
  phone,
  isAgreed,
  isLoading,
  onNameChange,
  onEmailChange,
  onPhoneChange,
  onIsAgreedChange,
  onSubmit,
}) => (
  <form className="tickets__form" onSubmit={isLoading ? null : onSubmit}>
    <div className="tickets__fieldset">
      <Modules.Input
        required
        name="name"
        value={name}
        autoComplete="name"
        placeholder="First Name"
        onChange={onNameChange}
        />

      <Modules.Input
        required
        name="email"
        value={email}
        autoComplete="email"
        placeholder="E-mail"
        onChange={onEmailChange}
        />
    </div>

    <div className="tickets__fieldset">
      <Modules.Input
        required
        type="tel"
        name="tel"
        value={phone}
        autoComplete="tel"
        placeholder="Phone"
        onChange={onPhoneChange}
        />

      <Modules.Checkbox
        checked={isAgreed}
        label="I confirm that I have read and agree to the Terms & Conditions and Cookie Policy"
        onChange={onIsAgreedChange}
        />
    </div>

    <div className="tickets__fieldset">
      <button className={cx('tickets__submit', {loading: isLoading})} disabled={isLoading} type="submit">Перейти к оплате</button>
    </div>
  </form>
);

export const enhance = compose(
  withStateHandlers(
    always({name: '', email: '', phone: '', isAgreed: false}), {
    onNameChange: () => ev => ({name: ev.target.value}),
    onEmailChange: () => ev => ({email: ev.target.value}),
    onPhoneChange: () => ev => ({phone: ev.target.value}),
    onIsAgreedChange: ({isAgreed}) => () => ({isAgreed: !isAgreed}),
  }),
  withHandlers({
    onSubmit: ({name, email, phone, onSubmit}) => ev => {
      ev.preventDefault();
      onSubmit({name, email, phone});
    },
  }),
  onlyUpdateForPropTypes,
  setPropTypes({
    name: T.string.isRequired,
    email: T.string.isRequired,
    phone: T.string.isRequired,
    isAgreed: T.bool.isRequired,
    isLoading: T.bool.isRequired,
    onNameChange: T.func.isRequired,
    onEmailChange: T.func.isRequired,
    onPhoneChange: T.func.isRequired,
    onIsAgreedChange: T.func.isRequired,
    onSubmit: T.func.isRequired,
  }),
);

export default enhance(Form);
