import {jarallax} from 'jarallax';

window.addEventListener('load', () => {
  jarallax(document.querySelectorAll('.jarallax'));
}, false);
