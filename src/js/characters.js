const $blocks = Array.prototype.slice.call(document.querySelectorAll('[data-characters-block]'));
const $triggers = Array.prototype.slice.call(document.querySelectorAll('[data-characters-trigger]'));

const ACTIVE_CLASS_NAME = 'active';
let activeBlock = 'main';

const onClick = value => () => {
  if (activeBlock === value) {
    activeBlock = 'main';
  } else {
    activeBlock = value;
  }

  $blocks.forEach($block =>
    $block.getAttribute('data-characters-block') === activeBlock ?
      $block.classList.add(ACTIVE_CLASS_NAME) :
      $block.classList.remove(ACTIVE_CLASS_NAME));

    $triggers.forEach($trigger =>
      $trigger.getAttribute('data-characters-trigger') === activeBlock ?
        $trigger.classList.add(ACTIVE_CLASS_NAME) :
        $trigger.classList.remove(ACTIVE_CLASS_NAME));
};

$triggers.forEach($trigger =>
  $trigger.addEventListener('click', onClick($trigger.getAttribute('data-characters-trigger')), {passive: true})
);
