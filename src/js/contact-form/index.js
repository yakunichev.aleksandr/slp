import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';

const $root = document.getElementById('contact-form-root');

export const unrender = () => ReactDOM.unmountComponentAtNode($root);

export const render = () => ReactDOM.render(<Root onClose={unrender}/>, $root);

const $trigger = document.querySelector('[data-contact-form-trigger]');

$trigger.addEventListener('click', ev => {
  ev.preventDefault();
  render();
}, false);
